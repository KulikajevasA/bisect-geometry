import THREE from "threejs";
import IDFAPP from "engine";
import Bisector from "./src/bisector";

const view3d = document.getElementById("view3d");
const engine = new IDFAPP.Engine({
    wrappers: {
        viewport: view3d
    },
    rendering: {
        logarithmicDepthBuffer: true
    }
});

const bisector = new Bisector();
const plane = new THREE.Plane();

setPlane(new THREE.Vector3(0.5, 0.2, 0), -0.01);

const sceneManager = engine.sceneManager;
const scene = new IDFAPP.Scenes.Scene(sceneManager);
sceneManager.scene = scene;

scene.camera.position.set(5, 5, 5);
scene.camera.lookAt(new THREE.Vector3());
scene._controls.update();

const lightDirectional = new THREE.DirectionalLight(0xcccccc, 0.5);
lightDirectional.position.set(1, 1, 1);

const lightDirectional2 = new THREE.DirectionalLight(0xcccccc, 0.5);
lightDirectional.position.set(-1, 1, 1);

const lightAmbient = new THREE.AmbientLight(0xffffee, 0.2);

// const geometry = new THREE.TorusKnotBufferGeometry(1, 0.1, 512, 512);
const geometry = new THREE.SphereBufferGeometry(1, 1024, 1024);
// const geometry = new THREE.BoxBufferGeometry(1, 1, 1);
// const geometry = new THREE.PlaneBufferGeometry(1, 1);
geometry.removeAttribute("uv");
const mesh = new THREE.Mesh(geometry, new THREE.MeshPhongMaterial({ color: 0xffffff, side: THREE.DoubleSide }));
const wireframe = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ color: 0x00ff00, wireframe: true, wireframeLinewidth: 2 }));

const helperPlane = new THREE.PlaneHelper(plane, 10);

// mesh.position.set(1, 1, 0);
mesh.add(wireframe);

scene.add(mesh, helperPlane, lightDirectional, lightDirectional2, lightAmbient);

wireframe.visible = false;
helperPlane.visible = false;

function bisect() {
    bisector.bisect(plane, mesh);

    setPlane(new THREE.Vector3(0.5, 0.2, 0), 0.01);
    bisector.bisect(plane, mesh);
    mesh.parent.remove(mesh.parent.children[1]);

    wireframe.visible = true;
    helperPlane.visible = true;

    geometry.dispose();
}

// bisect();

const planes = [
    new THREE.Plane(new THREE.Vector3(0.5, 0.2, 0).normalize(), 0.04),
    new THREE.Plane(new THREE.Vector3(-0.5, -0.2, 0).normalize(), 0.04),
    new THREE.Plane(new THREE.Vector3(0, 0, 1).normalize(), 0.4),
    new THREE.Plane(new THREE.Vector3(0, 0, -1).normalize(), 0.4),
    new THREE.Plane(new THREE.Vector3(0, 1, 0).normalize(), 0.2),
    new THREE.Plane(new THREE.Vector3(0, -1, 0).normalize(), 1)
];

planes.forEach(plane => {
    const helper = new THREE.PlaneHelper(plane, 2, Math.floor(Math.random() * 0xffffff));
    scene.add(helper);
});

bisector.bisectChunk(planes, mesh);

engine.view3d.beginRendering();

window.geometry = geometry;
window.planeHelper = helperPlane;
window.scene = scene;
window.bisect = bisect;


function setPlane(normal, constant) {
    plane.normal.copy(normal).normalize();
    plane.constant = constant;
}