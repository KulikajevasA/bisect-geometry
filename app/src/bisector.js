import THREE from "threejs";

class Bisector {
    constructor() { }

    /**
     * 
     * @param {THREE.Plane[]} planes 
     * @param {THREE.Mesh} mesh 
     */
    bisectChunk(planes, mesh) {

        const startTime = new Date().getTime();

        let activeMesh = mesh;
        let activeVertices = null;
        let activeNormals = null;

        const attrPositions = mesh.geometry.getAttribute("position");
        const attrNormals = mesh.geometry.getAttribute("normal");

        mesh.updateMatrixWorld();
        const inverse = new THREE.Matrix4().getInverse(mesh.matrixWorld);

        planes.forEach(plane => {
            tmpPlane.copy(plane).applyMatrix4(inverse);

            const geometry = activeMesh.geometry;
            const { indices, indicesOther, vertices, normals } = this._performBisection({ plane: tmpPlane, geometry, vertices: activeVertices, normals: activeNormals });

            activeVertices = vertices;
            activeNormals = normals;

            const attrIndices = geometry.getIndex();

            attrIndices.setArray(new Uint32Array(indices));
            attrIndices.needsUpdate = true;

            if (indicesOther.length > 0) {
                const geometry = new THREE.BufferGeometry();

                geometry.addAttribute("position", attrPositions);

                if (attrNormals) { geometry.addAttribute("normal", attrNormals); }

                geometry.setIndex(new THREE.Uint32BufferAttribute(indicesOther, 1));

                const otherMesh = new THREE.Mesh(geometry, mesh.material);

                mesh.add(otherMesh);
                activeMesh = otherMesh;
            }
        });

        mesh.remove(activeMesh);

        attrPositions.setArray(new Float32Array(activeVertices));
        attrPositions.needsUpdate = true;

        if (attrNormals) {
            attrNormals.setArray(new Float32Array(activeNormals));
            attrNormals.needsUpdate = true;
        }

        mesh.geometry.dispose();

        console.log(`\tTotal chunk bisection time: ${new Date().getTime() - startTime}ms.`);
    }

    /**
     * 
     * @param {THREE.Plane} plane 
     * @param {THREE.BufferGeometry} geometry 
     */
    _performBisection({ plane, geometry, vertices, normals }) {

        console.log("Performing bisection...");

        const attrIndices = geometry.getIndex();
        const attrPositions = geometry.getAttribute("position");
        const attrNormals = geometry.getAttribute("normal");
        const hasNormals = !!attrNormals;

        const startTime = new Date().getTime();
        const startIndices = attrIndices.count / 3;

        const mesh = new MeshBuilder(), separatedMesh = new MeshBuilder();

        const beginCopyVertices = new Date().getTime();
        vertices = vertices || attrPositions.array.reduce((acc, x) => { acc.push(x); return acc; }, []);
        normals = hasNormals ? (normals || attrNormals.array.reduce((acc, x) => { acc.push(x); return acc; }, [])) : null;

        console.log(`\tVertex copy time: ${new Date().getTime() - beginCopyVertices}ms.`);

        let indexCount = vertices.length / 3;

        const beginBisection = new Date().getTime();

        for (let i = 0, len = attrIndices.array.length; i < len; i += 3) {
            for (let j = 0; j < 3; j++) tmpIndices[j] = attrIndices.array[i + j];

            const acceptedCount = computeVertexToPlane(plane, tmpIndices, vertices, tmpVerts, tmpIsAccepted);

            if (acceptedCount !== 0) {
                if (acceptedCount !== 3) {
                    for (let j = 0; j < 3; j++) {
                        const hit = plane.intersectLine(tmpLines[j], tmpItsct[j]) || tmpLines[j].getCenter(tmpItsct[j]);

                        tmpIndices[3 + j] = indexCount++;
                        vertices.push(hit.x, hit.y, hit.z);

                        if (hasNormals) { computeNormals(PAIRS[j], tmpVerts, hit, tmpIndices, normals); }
                    }

                    splitEdges(tmpIndices, acceptedCount, tmpIsAccepted, mesh, separatedMesh);

                } else {
                    mesh.add(tmpIndices[0], tmpIndices[1], tmpIndices[2]);
                }
            } else {
                separatedMesh.add(tmpIndices[0], tmpIndices[1], tmpIndices[2]);
            }
        }

        console.log(`\tBisection time: ${new Date().getTime() - beginBisection}ms.`);

        console.log(`\tBisection total time: ${new Date().getTime() - startTime}ms. \n\tVertices: ${startIndices} => ${attrIndices.count / 3}`);

        return Object.freeze({
            indices: mesh.indices,
            indicesOther: separatedMesh.indices,
            vertices: vertices,
            normals: normals
        });
    }

    /**
     * @param {THREE.Plane} plane
     * @param {THREE.Mesh} mesh 
     */
    bisect(plane, mesh) {
        const group = new THREE.Group();

        mesh.matrixWorld.decompose(group.position, group.quaternion, group.scale);
        mesh.parent.add(group);
        mesh.position.set(0, 0, 0);
        mesh.quaternion.set(0, 0, 0, 1);
        mesh.scale.set(1, 1, 1);
        group.add(mesh);

        mesh.updateMatrixWorld();
        const inverse = new THREE.Matrix4().getInverse(mesh.matrixWorld);

        tmpPlane.copy(plane).applyMatrix4(inverse);

        const geometry = mesh.geometry;
        const { indices, indicesOther, vertices, normals } = this._performBisection({ plane: tmpPlane, geometry });

        const attrIndices = geometry.getIndex();
        const attrPositions = geometry.getAttribute("position");
        const attrNormals = geometry.getAttribute("normal");

        attrPositions.setArray(new Float32Array(vertices));
        attrIndices.setArray(new Uint32Array(indices));

        attrPositions.needsUpdate = true;
        attrIndices.needsUpdate = true;

        if (attrNormals) {
            attrNormals.setArray(new Float32Array(normals));
            attrNormals.needsUpdate = true;
        }

        if (indicesOther.length > 0) {
            const geometry = new THREE.BufferGeometry();

            geometry.addAttribute("position", attrPositions);
            geometry.addAttribute("normal", attrNormals);
            geometry.setIndex(new THREE.Uint32BufferAttribute(indicesOther, 1));

            group.add(new THREE.Mesh(geometry, mesh.material));
        }

        return group;
    }
}

export default Bisector;
export { Bisector };

const PAIRS = new Array([0, 1], [0, 2], [1, 2]);
const SLICES = new Array(
    [3, 4, 0],
    [2, 4, 5],
    [5, 4, 3],
    [3, 1, 5],
);
// const slices = acceptedCount === 2
//     ? [
//         [3, 4, 0],
//         [2, 4, 5],
//         [5, 4, 3],
//         [3, 1, 5],
//     ] : [
//         [3, 4, 0],
//         [2, 4, 5],
//         // [5, 4, 3],
//         [3, 1, 5],
//     ];

class Edge {
    /**
     * 
     * @param {Triangle} triangle 
     * @param {number} a 
     * @param {number} b 
     */
    constructor(triangle, a, b) {
        this.triangle = triangle;

        /**
         * @type {number}
         */
        this.v1 = null;

        /**
         * @type {number}
         */
        this.v2 = null;

        this.set(a, b);
    }

    /**
     * 
     * @param {number} a 
     * @param {number} b 
     */
    set(a, b) {
        this.v1 = a;
        this.v2 = b;
    }
}

const tmpVerts = new Array(new THREE.Vector3(), new THREE.Vector3(), new THREE.Vector3());
const tmpLines = PAIRS.map(pair => new THREE.Line3(tmpVerts[pair[0]], tmpVerts[pair[1]]));
const tmpItsct = new Array(new THREE.Vector3(), new THREE.Vector3(), new THREE.Vector3());
const tmpIsAccepted = new Array(3);
const tmpIndices = new Array(6);
const tmpPlane = new THREE.Plane();

class Triangle {
    /**
     * 
     * @param {number[]} index 
     */
    constructor(index) {
        const a = index[0], b = index[1], c = index[2];

        this.edge = PAIRS.map(pair => new Edge(this, index[pair[0]], index[pair[1]]));
        this.index = new Array(a, b, c);
    }
}

class MeshBuilder {
    constructor() {
        /**
         * @type {number[]}
         */
        this.indices = [];

        Object.freeze(this);
    }


    add(a, b, c) { this.indices.push(a, b, c); }
}

/**
 * @param {number[]} vertices
 * @param {number} acceptedCount
 * @param {number[]} isAccepted
 * @param {MeshBuilder} original
 * @param {MeshBuilder} separate
 */
function splitEdges(vertices, acceptedCount, isAccepted, original, separate) {
    SLICES.forEach(slice => {
        const a = slice[0], b = slice[1], c = slice[2];
        let holder = original;

        if ((isAccepted[a] === false || isAccepted[b] === false || isAccepted[c] === false) ||
            (acceptedCount === 1 && isAccepted[a] === undefined && isAccepted[b] === undefined && isAccepted[c] === undefined)) { holder = separate };

        holder.add(vertices[a], vertices[b], vertices[c]);
    });
}

function computeNormals(pair, verts, hit, indices, normals) {
    const pair1 = pair[0], pair2 = pair[1];

    const lenEdgeSq = verts[pair1].distanceToSquared(verts[pair2]);
    const lenHitSq = verts[pair1].distanceToSquared(hit);

    const ratioSq = lenHitSq / lenEdgeSq;
    const ratio = Math.sqrt(ratioSq);
    const invRatio = 1 - ratio;

    const offset1 = indices[pair1] * 3;
    const offset2 = indices[pair2] * 3;

    const x1 = normals[offset1], x2 = normals[offset2];
    const y1 = normals[offset1 + 1], y2 = normals[offset2 + 1];
    const z1 = normals[offset1 + 2], z2 = normals[offset2 + 2];

    const nx = ratio * x1 + invRatio * x2;
    const ny = ratio * y1 + invRatio * y2;
    const nz = ratio * z1 + invRatio * z2;

    normals.push(nx, ny, nz);
}

/**
 * 
 * @param {THREE.Plane} plane 
 * @param {number[]} indices 
 * @param {number[]} positions 
 * @param {THREE.Vector3[]} vertices 
 * @param {boolean[]} accepts 
 */
function computeVertexToPlane(plane, indices, positions, vertices, accepts) {
    let acceptedCount = 0;

    for (let j = 0; j < 3; j++) {
        vertices[j].fromArray(positions, indices[j] * 3);

        const dist = plane.distanceToPoint(vertices[j]);

        accepts[j] = false;
        if (dist < 0) {
            acceptedCount++;
            accepts[j] = true;
        }
    }

    return acceptedCount;
}