import "./extensions";

import { Engine, EventManager } from "./src/engine";
import { Physics } from "./src/physics";
import { Partition } from "./src/partition";

import { LoaderManager } from "./src/loaders/loader-man";
import { BaseLoader } from "./src/loaders/base/base-loader";
import { GLTFLoader } from "./src/loaders/gltf/gltf-loader";
import { OBJLoader } from "./src/loaders/obj/obj-loader";
import { TextureLoader } from "./src/loaders/texture/texture-loader";

import { ErcBackground } from "./src/objects/erc-background";
import { Projectable } from "./src/objects/projectable";

import { SceneManager } from "./src/scene/scene-man";
import { Scene } from "./src/scene/scene";
import { SampleScene } from "./src/scene/sample-scene";

const IDFAPP = {
    Engine: Engine,
    EventManager: EventManager,
    LoaderManager: LoaderManager,
    Loaders: {
        BaseLoader: BaseLoader,
        GLTFLoader: GLTFLoader,
        OBJLoader: OBJLoader,
        TextureLoader: TextureLoader
    },
    Physics: Physics,
    Objects: {
        ErcBackground: ErcBackground,
        Projectable: Projectable
    },
    Partition: Partition,
    SceneManager: SceneManager,
    Scenes: {
        Scene: Scene,
        SampleScene: SampleScene
    }
};

export { IDFAPP };
export default IDFAPP;