window.THREE = window.THREE || require("three");

var Config = {
    CAMERA: {
        FOV: 45,
        NEAR: 0.001,
        FAR: 5000,
        DEFAULT_POSITION: new THREE.Vector3(0, 0, 35),
        DEFAULT_TARGET: new THREE.Vector3()
    }
};

export { Config };