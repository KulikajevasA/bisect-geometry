import { Utils } from "./utils.js";

/**
 * Debug. This is a singleton class.
 */
class Debug {
    /**
     * Constructor for debugger.
     */
    constructor() {
        this._dom = document.getElementById("debug");
        this._values = {};

        this.hide();
    }

    /**
     * Hides debug container.
     */
    hide() {
        if (!this._dom) return;

        this._dom.style.display = "none";
        this._visible = false;
    }

    /**
     * Shows debug container.
     */
    show() {
        if (!this._dom) return;
        
        this._dom.style.display = "block";
        this._visible = true;
    }

    /**
     * Set debug value.
     * @param {String} name Name of the debug value.
     * @param {Object} value Debug value to print.
     */
    setValue(name, value) {
        this._values[name] = value;
    }

    /**
     * Remove value from debug prints.
     * @param {String} name Name of the debug to remove from the list.
     */
    clearValue(name) {
        delete this._values[name];
    }

    /**
     * Render the debug.
     */
    render() {
        if (this._visible && this._dom) {
            this._dom.innerHTML = "";
            Utils.forEach(this._values, function (val, name) {
                let li = document.createElement("li");
                li.innerText = "{0}: {1}".format(name, val);
                this._dom.appendChild(li);
            }, this);
        }
    }
};

if (!Debug._instance) {
    Debug._instance = null;
    Debug.getInstance = function () {
        if (!Debug._instance) {
            Debug._instance = new Debug();
        }

        return Debug._instance;
    };
}

const instance = Debug.getInstance();

export { instance as Debug };