import { View3d } from "./view3d";
import { EventManager } from "./events/events-man";
import { SceneManager } from "./scene/scene-man";
import { InputManager } from "./input/input-man";

/**
 * The engine itself.
 */
class Engine {
    /**
     * Constructor for engine.
     * @param {Object} params Engine parameters.
     */
    constructor(params) {
        this._params = params || {};
        this._view3d = new View3d(this);
        this._sceneManager = new SceneManager(this);
        this._inputManager = new InputManager(params.wrappers.viewport);

        if (!this._view3d.init()) return;

        window.focus();
    }

    /**
     * Getter for params.
     * @returns {Object} Parameters.
     */
    get params() {
        return this._params;
    }

    /**
     * Getter for scene manager.
     * @returns {SceneManager} Scene manager instance.
     */
    get sceneManager() {
        return this._sceneManager;
    }

    /**
     * Getter for input manager.
     * @returns {InputManager} Input manager instance.
     */
    get inputManager() {
        return this._inputManager;
    }

    /**
     * Getter for view 3d.
     * @returns {View3d} View 3d.
     */
    get view3d() {
        return this._view3d;
    }
}

export { Engine, EventManager, InputManager };