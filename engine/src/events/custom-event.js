/**
 * Custom event.
 */
class CustomEvent {
    /**
     * Custom event constructor.
     * @param {Event} original Original event data. 
     * @param {Any} custom Custom event data.
     */
    constructor(original, custom) {

        this._originalEvent = original === undefined ? null : original;
        this._customEvent = custom === undefined ? null : custom;
    }

    /**
     * Getter for original event data.
     * @returns {Event} Original event data.
     */
    get original() {
        return this._originalEvent;
    }

    /**
     * Getter for custom event data.
     * @returns {Any} Custom event data.
     */
    get custom() {
        return this._customEvent;
    }
}

export { CustomEvent };