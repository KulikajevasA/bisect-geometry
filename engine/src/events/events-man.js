const EventEmitter = require('events');

import { CustomEvent } from "./custom-event";

/**
 * Class responsible for event management. This is a singleton class.
 */
class EventManager extends EventEmitter {
    /**
     * Constructor for event manager.
     */
    constructor() {
        super();
    }

    /**
     * Trtiggers a custom event.
     * @param {String} name Name of the event to trigger.
     * @param {Event} original Original event data.
     * @param {Any} custom Custom event data.
     */
    trigger(name, original, custom) {
        super.emit(name, new CustomEvent(original, custom));
    }
}

if (!EventManager._instance) {
    EventManager._instance = null;
    EventManager.getInstance = function () {
        if (!EventManager._instance) {
            EventManager._instance = new EventManager();
        }

        return EventManager._instance;
    };
}

const instance = EventManager.getInstance();

export { instance as EventManager, CustomEvent };