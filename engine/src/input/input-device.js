import { InputManager } from "./input-man";

/**
 * Base class for input device.
 */
class InputDevice {
    /**
     * Constructor.
     * @param {InputManager} manager Input manager.
     * @param {Element} wrapper Event wrapper.
     */
    constructor(manager, wrapper) {
        this._manager = manager;
        this._wrapper = wrapper;
    }

    /**
     * Retrieves a copy of current mouse state.
     * @returns {Object} Gets current state.
     * @throws {Error} Not implemented exception.
     */
    getState() {
        throw new Error("Must be implemented by inheriting class.");
    }
}

export { InputDevice }