import { Mouse, MouseButtons_T } from "./mouse";
import { Keyboard, KeyboardKeys_T } from "./keyboard";

/**
 * Manager for user inputs.
 */
class InputManager {
    /**
     * Constructor.
     * @param {Element} wrapper Event wrapper.
     */
    constructor(wrapper) {
        this._mouse = new Mouse(this, wrapper);
        this._keyboard = new Keyboard(this, wrapper);
    }

    /**
     * Retrieve current input state.
     */
    get state() {
        return {
            mouse: this._mouse.getState(),
            keyboard: this._keyboard.getState()
        };
    }
};

export { InputManager, Mouse, MouseButtons_T, Keyboard, KeyboardKeys_T };