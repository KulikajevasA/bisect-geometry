import { InputManager } from "./input-man";
import { InputDevice } from "./input-device";
import { Utils } from "../../utils";
import { EventManager, CustomEvent } from "../events/events-man";

/**
 * Handles keyboard inputs.
 */
class Keyboard extends InputDevice {
    /**
     * Constructor.
     * @param {InputManager} manager Input manager.
     * @param {Element} wrapper Event wrapper.
     */
    constructor(manager, wrapper) {
        super(manager, wrapper);

        this._states = {};
        this._lastState = null;

        this._clearState();

        window.addEventListener("keydown", this._handleKeyDown.bind(this), false);
        window.addEventListener("keyup", this._handleKeyUp.bind(this), false);
    }

    /**
     * Gets key state.
     * @returns {Object} Key states.
     */
    getState() {
        return Object.assign({}, this._states);
    }

    /**
     * Clears event state.
     */
    _clearState() {
        Utils.forEach(KeyboardKeys_T, function (val) {
            this._states[val] = false;
        }, this);

        this._lastState = this.getState();
    }

    /**
     * Checks if key is valid.
     * @param {Number} keycode Keycode.
     * @returns {Boolean} TRUE if valid, FALSE otherwise.
     */
    _isValid(keycode) {
        return this._states[keycode] !== undefined;
    }

    /**
     * Sets key state.
     * @param {Number} keycode Key to set state of.
     * @param {Boolean} state State to set for the key.
     */
    _setKeyState(keycode, state) {
        if (!this._isValid(keycode)) return;

        this._states[keycode] = state;
    }

    /**
     * Triggers state change event.
     * @param {Event} evt Event data.
     */
    _trigger(evt) {
        this._lastState = this.getState();

        EventManager.trigger(evt.name || evt.type, evt, this._lastState);
    }

    /**
     * Handler for keydown event.
     * @param {Event} evt Event data.
     */
    _handleKeyDown(evt) {
        this._setKeyState(evt.keyCode || evt.which, true);
        this._trigger(evt);
    }

    /**
     * Handler for keyup event.
     * @param {Event} evt Event data.
     */
    _handleKeyUp(evt) {
        this._setKeyState(evt.keyCode || evt.which, false);
        this._trigger(evt);
    }

    /**
     * Is control key down.
     * @returns {Boolean} TRUE if down, FALSE othwerise.
     */
    isControlDown() {
        return this._states[KeyboardKeys_T.CTRL] || false;
    }

    /**
     * Is alt key down.
     * @returns {Boolean} TRUE if down, FALSE otherwise.
     */
    isAltDown() {
        return this._states[KeyboardKeys_T.ALT] || false;
    }
}

/**
 * Keyboard keys.
 */
const KeyboardKeys_T = {
    W: 87,
    A: 65,
    S: 83,
    D: 68,
    ALT: 18,
    CTRL: 17
};

export { Keyboard, KeyboardKeys_T };