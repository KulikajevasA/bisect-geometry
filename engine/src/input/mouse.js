import THREE from "threejs";
import { Utils } from "../../utils";
import { InputManager } from "./input-man";
import { InputDevice } from "./input-device";
import { EventManager, CustomEvent } from "../events/events-man";

/**
 * Handles mouse inputs.
 */
class Mouse extends InputDevice {
    /**
     * Constructor.
     * @param {InputManager} manager Input manager.
     * @param {Element} wrapper Event wrapper.
     */
    constructor(manager, wrapper) {
        super(manager, wrapper);

        this._states = [];
        this._lastMousePos = new THREE.Vector2();
        this._bounds = null;
        this._lastState = null;

        this._clearState();

        wrapper.addEventListener("mousedown", this._onMouseDown.bind(this), false);
        wrapper.addEventListener("mouseup", this._onMouseUp.bind(this), false);
        wrapper.addEventListener("mousemove", this._onMouseMove.bind(this), false);
        wrapper.addEventListener("click", this._onMouseClick.bind(this), false);

        document.addEventListener("focusout", this._onLooseFocus.bind(this), false);
        window.addEventListener("blur", this._onLooseFocus.bind(this), false);
        wrapper.addEventListener("mouseleave", this._onLooseFocus.bind(this), false);
        wrapper.addEventListener("focusout", this._onLooseFocus.bind(this), false);
        wrapper.addEventListener("blur", this._onLooseFocus.bind(this), false);

        EventManager.on("resize", this._onResize.bind(this));
    }

    /**
     * Retrieves a copy of current mouse state.
     * @returns {Object} Gets current state.
     */
    getState() {
        return {
            states: this._states.slice(),
            position: this._lastMousePos.clone()
        };
    }

    /**
     * Helper function to trigger event.
     * @param {Event} evt Original event.
     */
    _trigger(evt) {
        window.focus();

        this._lastState = this.getState();

        EventManager.trigger(evt.type || evt.name, evt, this._lastState);
    }

    /**
     * Handler for on mouse down event.
     * @param {Event} evt Original event.
     */
    _onMouseDown(evt) {
        if (!evt.which)
            throw new Error("Browser does not support 'which':", evt);

        this._states[evt.which] = true;

        this._setXYPosition(evt);

        this._trigger(evt);
    }

    /**
     * Handler for mouse up event.
     * @param {Event} evt Original event.
     */
    _onMouseUp(evt) {
        if (!evt.which)
            throw new Error("Browser does not support 'which':", evt);

        this._states[evt.which] = false;

        this._setXYPosition(evt);

        this._trigger(evt);
    }

    /**
     * Handler for mouse move event.
     * @param {Event} evt Original event.
     */
    _onMouseMove(evt) {
        this._setXYPosition(evt);

        this._trigger(evt);
    }

    /**
     * Handler for mouse click event.
     * @param {Event} evt Original event.
     */
    _onMouseClick(evt) {
        if (!evt.which)
            throw new Error("Browser does not support 'which':", evt);

        this._setXYPosition(evt);

        this._trigger(evt);
    }

    /**
     * Handler for focus loss event.
     * @param {Event} evt Original event.
     */
    _onLooseFocus(evt) {
        this._clearState();

        this._lastState = this.getState();
        EventManager.trigger("mouseup", null, this._lastState);
    }

    /**
     * Clears event state.
     */
    _clearState() {
        Utils.forEach(MouseButtons_T, function (val) {
            this._states[val] = false;
        }, this);

        this._lastState = this.getState();
    }

    /**
     * Sets last mouse position.
     * @param {Event} evt Event data.
     */
    _setXYPosition(evt) {
        if (!this._bounds) return;

        var x = evt.clientX - this._bounds.left;
        var y = evt.clientY - this._bounds.top;

        var nx = ((2 * x) / this._bounds.width - 1);
        var ny = (-(2 * y) / this._bounds.height + 1);

        this._lastMousePos.set(nx, ny);
    }

    /**
     * Handler for resize event.
     * @param {CustomEvent} evt Event data.
     */
    _onResize(evt) {
        this._bounds = evt.custom;
    }
};

/**
 * Mouse buttons.
 */
const MouseButtons_T = {
    LEFT: 1,
    MIDDLE: 2,
    RIGHT: 3
};

export { Mouse, MouseButtons_T };