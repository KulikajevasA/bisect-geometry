import THREE from "threejs";
import { Utils } from "../../../utils";
import { LoaderManager } from "../loader-man";

/**
 * Base loading class.
 */
class BaseLoader {
    /**
     * Constructor.
     */
    constructor() {
        this._fileLoader = new THREE.FileLoader();
        this._manager = null;
    }

    /**
     * Setter for manager.
     * @param {LoaderManager} manager Set the manager.
     */
    set manager(manager) {
        this._manager = manager;
    }

    /**
     * Initializes the geometry loader.
     * @throws {Error} Not implemented exception.
     */
    init() {
        throw Error("Must be implemented by inheriting class.");
    }

    /**
     * Loads the file.
     * @param {String} url Download URL.
     * @param {Object} params Deserialization parameters.
     * @returns {Promise} Promise.
     */
    load(url, params = {}) {

        const promise = new Promise(function (resolve, reject) {
            this._fileLoader.load(url,
                this._onDownloadComplete.bind(this, resolve, reject, params),
                undefined, reject);
        }.bind(this));

        if (this._manager) this._manager.registerLoad(promise);
        
        promise.then(function () { this._manager.notifySuccess(); }.bind(this))
            .catch(function () { this._manager.notifyFail(); }.bind(this));    

        return promise;
    };

    /**
     * On download complete callback.
     * @param {Function} resolve Resolve promise.
     * @param {Function} reject Reject promise.
     * @param {Object} params Deserialization parameters.
     * @param {Object} data Downloaded data.
     */
    _onDownloadComplete(resolve, reject, params, data) {
        throw new Error("Must be implemented by inheriting class.");
    }
}

export { BaseLoader }