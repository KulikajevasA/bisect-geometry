import THREE from "threejs";
import GLTF2Loader from "three-gltf2-loader";
import { BaseLoader } from "../base/base-loader";
import { Utils } from "../../../utils";

if (!THREE.GLTF2Loader) GLTF2Loader(window.THREE);

/**
 * GLTF loader.
 */
class GLTFLoader extends BaseLoader {
    /**
     * Constructor for GLTF loader.
     */
    constructor() {
        super();

        this._fileLoader = new THREE.GLTF2Loader();
    }

    /**
     * Init.
     */
    init() { }

    /**
     * On download complete callback.
     * @param {Function} resolve Resolve promise.
     * @param {Function} reject Reject promise.
     * @param {Object} params Deserialization parameters.
     * @param {Object} data Downloaded data.
     */
    _onDownloadComplete(resolve, reject, params, data) {
        var inActionMap = {}, outActionMap = {};

        //Prepare animation fix
        while (data.animations.length > 0) {
            let action = data.animations.pop();
            let tracks = {};

            for (let i = 0; i < action.tracks.length; i++) {
                let track = action.tracks[i];
                let group = track.name.split(/\.(.+)/);
                let targetName = group[0];
                let targetProperty = group[1];
                let trackData = tracks[targetName] = tracks[targetName] || [];

                trackData.push({
                    times: track.times,
                    values: track.values,
                    property: targetProperty,
                    constructor: track.constructor,
                    interpolation: track.getInterpolation()
                });
            }

            inActionMap[action.name] = { original: action.tracks, prepared: tracks };
        }

        function enableCollision(object, i) {
            if (object instanceof THREE.Mesh) {

                //Create fixed mesh animations
                let objectName = "{0}_mesh{1}".format(object.name, i);
                Utils.forEach(inActionMap, (action, name) => {
                    if (action.prepared[object.name]) {
                        Utils.forEach(action.prepared[object.name], (track) => {
                            //Do not copy these animations
                            switch (track.property) {
                                case "position":
                                case "rotation":
                                case "quaternion":
                                case "scale":
                                    return;
                            }

                            let trackName = "{0}.{1}".format(objectName, track.property);
                            let outAction = outActionMap[name] = outActionMap[name] || [];
                            let times = track.times;
                            let values = track.values;
                            let interpolation = track.interpolation;
                            let outTrack = new track.constructor(trackName, times, values, interpolation);

                            outAction.push(outTrack);
                        });
                    }
                });

                object.name = objectName;
                object.isSelectable = true;
            }

            Utils.forEach(object.children, enableCollision);
        }

        Utils.forEach(data.scenes, enableCollision);

        //Push fixed animations
        Utils.forEach(inActionMap, (tracks, name) => {
            let extraTracks = outActionMap[name] || [];

            data.animations.push(new THREE.AnimationClip(name, undefined, tracks.original.concat(extraTracks)));
        });

        resolve(data);
    }
}

export { GLTFLoader };