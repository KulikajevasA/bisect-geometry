import THREE from "threejs";
import { BaseLoader } from "../base/base-loader";
import { Utils } from "../../../utils";

class JSONLoader extends BaseLoader {
    constructor() {
        super();

        this._fileLoader = new THREE.ObjectLoader();
    }

    init() { }

    _onDownloadComplete(resolve, reject, params, data) {
        resolve(data);
    }
}

export { JSONLoader };