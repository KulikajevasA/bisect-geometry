import { Utils } from "../../utils";
import { EventManager } from "../events/events-man";
import { BaseLoader } from "./base/base-loader";

/**
 * Loader manager.
 */
class LoaderManager {
    /**
     * Constructor.
     * @param {Array} loaders List of loaders to manage.
     */
    constructor(loaders) {
        loaders = loaders instanceof Array ? loaders : [loaders];

        Utils.forEach(loaders, function (loader) {
            loader.manager = this;
            loader.init();
        }, this);

        this._loading = 0;
        this._successful = 0;
        this._failed = 0;
    }

    /**
     * Registers a loading asset.
     */
    registerLoad() {
        this._loading++;
    }

    /**
     * Registers asset load success.
     */
    notifySuccess() {
        this._successful++;
        this._decrementLoading();
    }

    /**
     * Decrements loading.
     */
    _decrementLoading() {
        if (this._loading == 1) this._resolve();

        this._loading = Math.max(this._loading - 1, 0);
    }

    /**
     * Notifies asset load failure.
     */
    notifyFail() {
        this._failed++;
        this._decrementLoading();
    }


    /**
     * All assets loaded.
     */
    _resolve() {
        EventManager.trigger("load_finished", null, {
            loader: this,
            data: {
                successful: this._successful,
                failed: this._failed
            }
        });

        this._loading = 0;
        this._successful = 0;
        this._failed = 0;
    }
}

export { LoaderManager };