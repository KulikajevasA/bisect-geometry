import THREE from "threejs";
import { BaseLoader } from "../base/base-loader";
import { Utils } from "../../../utils";

class OBJLoader extends BaseLoader {
    constructor() {
        super();

        this._fileLoader = new THREE.OBJLoader();
    }

    init() { }

    _onDownloadComplete(resolve, reject, params, data) {
        resolve(data);
    }
}

export { OBJLoader };
export default OBJLoader;