import THREE from "threejs";
import { BaseLoader } from "../base/base-loader";
import { Utils } from "../../../utils";

class TextureLoader extends BaseLoader {
    constructor() {
        super();

        this._fileLoader = new THREE.TextureLoader();
    }

    init() { }

    _onDownloadComplete(resolve, reject, params, data) {
        resolve(data);
    }
}

export { TextureLoader };
export default TextureLoader;