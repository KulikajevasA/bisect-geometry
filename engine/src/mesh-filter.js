import THREE from "threejs";
import { Utils } from "../utils";

const FACES_PER_SPLIT = 100000;
//const FACES_PER_SPLIT = 10000;

/**
 * Filter contant.
 */
class FilterOctant extends THREE.Box3 {
    /**
     * Constructor.
     * @param {THREE.Vector3} min Octant min bound.
     * @param {THREE.Vector3} max Octant max bound.
     * @param {Number} depth Octant depth.
     */
    constructor(min = new THREE.Vector3(), max = new THREE.Vector3(), depth = 0) {
        super(min, max);

        this._children = null;
        this._faces = [];
        this._depth = depth;
    }

    /**
     * Splits octant.
     */
    split() {
        if (this._children) return;

        var min = this.min;
        var max = this.max;
        var mid = this.getCenter();

        var octants = [
            null, null,
            null, null,
            null, null,
            null, null
        ];

        let combination;

        for (let i = 0; i < 8; ++i) {
            combination = SPLIT_PATTERN[i];
            octants[i] = new FilterOctant(
                new THREE.Vector3(
                    (combination[0] === 0) ? min.x : mid.x,
                    (combination[1] === 0) ? min.y : mid.y,
                    (combination[2] === 0) ? min.z : mid.z
                ),

                new THREE.Vector3(
                    (combination[0] === 0) ? mid.x : max.x,
                    (combination[1] === 0) ? mid.y : max.y,
                    (combination[2] === 0) ? mid.z : max.z
                ),

                this._depth + 1
            );
        }

        this._children = octants;

        //Transfer children
        while (this._faces.length > 0) {
            let face = this._faces.pop();

            this.addFace(face);
        }
    }

    /**
     * Adds a face to octant, splits if necessary.
     * @param {THREE.Vector3[]} face Face to add.
     */
    addFace(face) {
        let contains = this.containsPoint(face[0]) || this.containsPoint(face[1]) || this.containsPoint(face[2]);

        if (!contains) return;

        if (!this._children && this._faces.length + 1 > FACES_PER_SPLIT) {
            this.split();
        }

        if (this._children) {
            for (let i = 0; i < this._children.length; i++) {
                let octant = this._children[i];
                octant.addFace(face);
            }
        } else {
            this._faces.push(face);
        }
    }

    /**
     * Intersects ray with octant.
     * @param {THREE.Ray} ray Ray to intersect.
     * @param {THREE.Vector3} origin Ray origin.
     * @param {THREE.BackSide|THREE.FrontSide|THREE.DoubleSide} side Triangle side.
     * @param {THREE.Matrix4} matrixWorld Object world matrix.
     * @param {Number} near Near plane.
     * @param {Number} far Far plane.
     * @returns {THREE.Intersection} Intersection.
     */
    intersectRay(ray, origin, side, matrixWorld, near, far) {
        var isIntersecting = ray.intersectsBox(this);

        var out = null;

        if (!isIntersecting) return out;

        if (this._children) {
            for (let i = 0; i < this._children.length; i++) {
                let ch = this._children[i];

                let chIntersects = ch.intersectRay(ray, origin, side, matrixWorld, near, far);

                if (chIntersects) {
                    if (!out) {
                        out = chIntersects
                    } else if (out.distance > chIntersects.distance)
                        out = chIntersects;
                }
            }
        } else {
            var intersectionPoint = new THREE.Vector3();

            for (let i = 0; i < this._faces.length; i++) {
                let face = this._faces[i];
                let a = face[0], b = face[1], c = face[2];

                let itsct = null;

                if (side === THREE.BackSide)
                    itsct = ray.intersectTriangle(c, b, a, true, intersectionPoint);
                else itsct = ray.intersectTriangle(a, b, c, side !== THREE.DoubleSide, intersectionPoint);

                if (itsct) {
                    intersectionPoint.applyMatrix4(matrixWorld);

                    let distance = origin.distanceTo(intersectionPoint);

                    if (distance < near || distance > far) continue;

                    if (!out) {
                        out = {
                            point: intersectionPoint.clone(),
                            distance: distance
                        }
                    } else {
                        if (out.distance > distance) {
                            out.distance = distance;
                            out.point.copy(intersectionPoint);
                        }
                    }
                }
            }

            out = out;
        }

        return out;
    }
}

/**
 * Octree based mesh filter.
 */
class MeshFilter {
    /**
     * Constructor.
     * @param {THREE.Mesh} mesh Mesh to filter.
     */
    constructor(mesh) {
        if (!mesh.geometry.boundingBox) mesh.geometry.computeBoundingBox();

        var boundingBox = mesh.geometry.boundingBox;

        this._root = new FilterOctant(boundingBox.min, boundingBox.max);

        this._mesh = mesh;

        this._init();
    }

    /**
     * Initializes the mesh filter.
     */
    _init() {
        var geometry = this._mesh.geometry;

        if (geometry instanceof THREE.BufferGeometry) {
            //Pre-split, because we will need to anyway.
            if (geometry.index.count > FACES_PER_SPLIT)
                this._root.split();

            this._createFromBufferGeometry(geometry);

        } else {
            throw new Error("Not yet implemented.");
        }
    }

    /**
     * Creates filter from buffer geometry.
     * @param {THREE.BufferGeometry} geometry Geometry to filter. 
     */
    _createFromBufferGeometry(geometry) {
        var faces = geometry.index;
        var faceCount = faces.count;
        var vertices = geometry.attributes.position.array;

        for (let i = 0; i < faceCount; i += 3) {
            let a = faces.array[i] * 3;
            let b = faces.array[i + 1] * 3;
            let c = faces.array[i + 2] * 3;

            let a1 = vertices[a], a2 = vertices[a + 1], a3 = vertices[a + 2];
            let b1 = vertices[b], b2 = vertices[b + 1], b3 = vertices[b + 2];
            let c1 = vertices[c], c2 = vertices[c + 1], c3 = vertices[c + 2];

            let va = new THREE.Vector3(a1, a2, a3);
            let vb = new THREE.Vector3(b1, b2, b3);
            let vc = new THREE.Vector3(c1, c2, c3);

            let face = [va, vb, vc];

            this._root.addFace(face);
        }
    }

    /**
     * Intersects raycaster with mesh filter.
     * @param {THREE.Raycaster} raycaster Raycaster to intersect.
     * @returns {THREE.Intersection} Intersection result.
     */
    intersect(raycaster) {
        var obj = this._mesh;
        var ray = new THREE.Ray();
        var matrixWorld = this._mesh.matrixWorld;
        var inverseMatrix = new THREE.Matrix4().getInverse(matrixWorld);

        ray.copy(raycaster.ray).applyMatrix4(inverseMatrix);

        var intersect = this._root.intersectRay(ray, raycaster.ray.origin, obj.material.side, matrixWorld, raycaster.near, raycaster.far);

        if (intersect) intersect.object = obj;


        return intersect;
    }
}

/**
 * Helper to visualize mesh filter.
 */
class MeshFilterHelper extends THREE.Group {
    /**
     * Constructor.
     * @param {MeshFilter} filter Mesh filter to visualize.
     */
    constructor(filter = null) {
        super();

        var indices = new Uint16Array([0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4, 0, 4, 1, 5, 2, 6, 3, 7]);
        this._indiceAttribute = new THREE.BufferAttribute(indices, 1)

        var helperRoot = this;

        var bb = new THREE.Box3();
        var color = 0xffffff * Math.random();

        var segFunc = this._createSegments.bind(this);

        function createLevel(tree) {

            bb.copy(tree);

            helperRoot.add(segFunc(bb, color));

            if (tree._children) {
                color = 0xffffff * Math.random();
                Utils.forEach(tree._children, function (node) {
                    createLevel(node);
                });
            }
        }

        if (filter) createLevel(filter._root);
    }

    /**
     * Creates segments for bounds.
     * @param {THREE.Box3} bounds Bounding box.
     * @param {Number} color Color.
     */
    _createSegments(bounds, color) {
        var min = bounds.min, max = bounds.max;

        var positions = new Float32Array(8 * 3);

        var geometry = new THREE.BufferGeometry();
        geometry.setIndex(this._indiceAttribute);
        geometry.addAttribute('position', new THREE.BufferAttribute(positions, 3));

        var position = geometry.attributes.position;
        var array = position.array;

        array[0] = max.x; array[1] = max.y; array[2] = max.z;
        array[3] = min.x; array[4] = max.y; array[5] = max.z;
        array[6] = min.x; array[7] = min.y; array[8] = max.z;
        array[9] = max.x; array[10] = min.y; array[11] = max.z;
        array[12] = max.x; array[13] = max.y; array[14] = min.z;
        array[15] = min.x; array[16] = max.y; array[17] = min.z;
        array[18] = min.x; array[19] = min.y; array[20] = min.z;
        array[21] = max.x; array[22] = min.y; array[23] = min.z;

        position.needsUpdate = true;

        return new THREE.LineSegments(geometry, new THREE.LineBasicMaterial({ color: color }));
    }
}

export { MeshFilter, MeshFilterHelper };

/**
 * A binary pattern that describes the standard octant layout:
 *
 * ```text
 *    3____7
 *  2/___6/|
 *  | 1__|_5
 *  0/___4/
 * ```
 *
 * This common layout is crucial for positional assumptions.
 *
 * @type {Uint8Array[]}
 */
const SPLIT_PATTERN = [

    new Uint8Array([0, 0, 0]),
    new Uint8Array([0, 0, 1]),
    new Uint8Array([0, 1, 0]),
    new Uint8Array([0, 1, 1]),

    new Uint8Array([1, 0, 0]),
    new Uint8Array([1, 0, 1]),
    new Uint8Array([1, 1, 0]),
    new Uint8Array([1, 1, 1])
];