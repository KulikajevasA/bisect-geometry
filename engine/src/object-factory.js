import { Utils } from "./utils";

/**
 * Object factory template.
 */
class ObjectFactory {
    /**
     * Constructor for object factory.
     * First argument is the prototype to construct for.
     * Further parameters are arguments to construct object with.
     */
    constructor() {
        if (arguments.length < 0) throw new Error("Constructor not specified as the first parameter.");

        this._arguments = [];

        Utils.forEach(arguments, function (arg) {
            this._arguments.push(arg);
        }, this);
    }

    /**
     * Creates an instance with specified arguments.
     */
    build() {
        var constructor = this._arguments[0];
        var extraArgs = [];

        Utils.forEach(arguments, function (arg) {
            extraArgs.push(arg);
        }, this);

        var args = this._arguments.concat(extraArgs);
        var InstanceType = constructor.bind.apply(constructor, args);

        return new InstanceType();
    }
}

export { ObjectFactory };