import THREE from "threejs";

/**
 * Equirectangular background.
 */
class ErcBackground extends THREE.Mesh {
    /**
     * Constructor for equirectangular background.
     * @param {String} image Path to the background image.
     * @param {Number} radius Radius of the sphere.
     * @param {Number} segments Number of segments for realistic projection.
     */
    constructor(image = null, radius = 256, segments = 128) {
        super(new THREE.SphereBufferGeometry(radius, segments, segments),
            new THREE.MeshBasicMaterial({ side: THREE.DoubleSide }));

        this._textureLoader = null;

        this.setImage(image);
    }

    /**
     * Set image as new background.
     * @param {String} image Path to the background image.
     */
    setImage(image) {
        if (image) {
            this._textureLoader = this._textureLoader || new THREE.TextureLoader();

            this._textureLoader.load(image, function (texture) {
                this.material.map = texture;
                this.material.needsUpdate = true;
            }.bind(this));
        } else {
            this.material.map = null;
            this.material.needsUpdate = true;
        }

        this.material.color.setHex(0xffffff);
    }

    /**
     * Clears image and sets map as color.
     * @param {Number} hex Hex color to set background to.
     */
    setColor(hex) {
        this.material.map = null;
        this.material.color.setHex(hex);
        this.material.needsUpdate = true;
    }
}

export { ErcBackground };