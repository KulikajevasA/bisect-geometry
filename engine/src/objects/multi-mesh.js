import THREE from "threejs";
import { Utils } from "../../utils";

/**
 * Mesh that supports multiple geometries at once.
 */
class MultiMesh extends THREE.Group {
    /**
     * Constructor for multimesh.
     * @param {Array} geometries 
     * @param {THREE.Material} material 
     */
    constructor(geometries, material) {
        super();

        this._geometries = geometries;
        this._material = material;

        this._isSelectable = false;

        this._meshes = [];

        Utils.forEach(geometries, function (geometry) {
            let mesh = new THREE.Mesh(geometry, this.material);
            this._meshes.push(mesh);
            this.add(mesh);
        }, this);
    }

    /**
     * Setter for isSelectable.
     */
    set isSelectable(selectable) {
        this._isSelectable = selectable;

        Utils.forEach(this._meshes, function (child) {
            child.isSelectable = selectable;
        }, this);
    }

    /**
     * Getter for isSelectable.
     */
    get isSelectable() {
        return this._isSelectable;
    }

    /**
     * Retrieve the current material.
     */
    get material() {
        return this._material;
    }

    /**
     * Set the current material.
     */
    set material(material) {
        this._material = material;

        Utils.forEach(this._meshes, function (child) {
            child.material = this._material;
        }, this);
    }
}

export { MultiMesh };