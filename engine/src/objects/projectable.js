import THREE from "threejs";

import { EventManager } from "../events/events-man";

/**
 * Projects object position from 3D space to viewport.
 */
class Projectable extends THREE.Group {
    /**
     * Constructor.
     * @param {Object} dims Current dimensions.
     */
    constructor(dims) {
        super();

        this.createHelper();

        var size = this._size = new THREE.Vector2();
        var offset = this._offset = new THREE.Vector2();

        if(dims) {
            size.set(dims.width, dims.height);
            offset.set(dims.left, dims.top);
        }

        EventManager.on("resize", (evt) => {
            let dims = evt.custom;

            size.set(dims.width, dims.height);
            offset.set(dims.left, dims.top);
        });
    }

    /**
     * Creates helper.
     */
    createHelper() {
        var helper = new THREE.Mesh(new THREE.BoxBufferGeometry(0.5, 0.5, 0.5), new THREE.MeshPhongMaterial({ color: 0xff0000 }));

        this.add(helper);

        return helper;
    }

    /**
     * Project to 2D.
     * @param {THREE.Camera} camera Camera to project from.
     * @returns {THREE.Vector2} Projected position.
     */
    project(camera) {
        var tmp = new THREE.Vector3();
        var size = this._size;
        var offset = this._offset;

        function project(vec3) {
            tmp.copy(vec3);
            tmp.project(camera);

            var px = ((size.x * tmp.x) + size.x) / 2;
            var py = (size.y - (size.y * tmp.y)) / 2;

            return new THREE.Vector2(Math.round(px), Math.round(py));
        }

        return project(this.getWorldPosition());
    }
}

export { Projectable };