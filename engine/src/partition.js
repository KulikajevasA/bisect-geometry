var BxH = require("bxh"),
    BIH = BxH.BIH,
    BVH = BxH.BVH,
    AABB = BxH.AABB,
    Ray = BxH.Ray,
    IntersectInfo = BxH.IntersectInfo;

import THREE from "threejs";
import { Utils } from "../utils";
import { Debug } from "../debug";
import { Physics } from "./physics";
import { MeshFilter, MeshFilterHelper } from "./mesh-filter";

class LeafObject {
    /**
     * 
     * @param {THREE.Mesh} object
     */
    constructor(object) {
        this._object = object;
        this._bbox = new THREE.Box3();

        this.computeBoundingBox();
    }

    /**
     * @returns {THREE.Box3}
     */
    get boundingBox() {
        return this._bbox;
    }

    /**
     * @returns {THREE.Mesh}
     */
    get object() {
        return this._object;
    }

    /**
     * @returns {THREE.Box3}
     */
    computeBoundingBox() {
        this._bbox.setFromObject(this._object);

        return this._bbox;
    }
}

class ExtendedAABB extends AABB {
    constructor(min, max) {
        super(min, max);

        this._boundingBox = new THREE.Box3().setFromArray(min.concat(max));
    }

    get boundingBox() {
        return this._boundingBox;
    }
};

class NodeBxH extends LeafObject {
    constructor(object, filter) {
        super(object);

        this._filter = filter;

        var geometry = object.geometry;
        var faces = geometry instanceof THREE.BufferGeometry
            ? geometry.index.count / 3
            : geometry.faces.length;


        this._aabb = new ExtendedAABB(this._bbox.min.toArray(), this._bbox.max.toArray());

        this._weight = faces;
    }

    getAABB() {
        return this._aabb;
    }

    getWeight() {
        return this._weight;
    }

    overlaps(aabb) {
        return this.boundingBox.intersectsBox(aabb.boundingBox);
    }

    contained(aabb) {
        return this.boundingBox.containsBox(aabb.boundingBox);
    }

    contains(aabb) {
        return contained(aabb);
    }

    intersect(ray, intersectInfo) {

        let parent = this._object;
        let visible = true;

        while (parent && visible) {
            visible &= parent.visible;
            parent = parent.parent;
        }

        if (!visible) return;

        var _ray = Physics.getRaycaster().ray;

        var rs = this._aabb.intersectWithSegment(ray.toIntervals());

        intersectInfo.tests++;

        if (rs) {
            let p = new THREE.Vector2(rs[0].a, rs[1].a);
            let rayPosition = new THREE.Vector2().fromArray(ray.position);
            let dir = new THREE.Vector2().subVectors(p, rayPosition);
            var t = dir.length();

            if (t <= ray.maxT && t >= ray.minT) {
                if (_ray.intersectsBox(this.boundingBox)) {
                    intersectInfo.position = p;

                    intersectInfo.isHit = true;
                    intersectInfo.hits++;

                    intersectInfo.nodes.push(this._filter);
                    //ray.minT = t;
                }
            }
        }
    }
}

class BxVHelper {
    constructor(bxh = null) {
        var indices = new Uint16Array([0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4, 0, 4, 1, 5, 2, 6, 3, 7]);
        this._indiceAttribute = new THREE.BufferAttribute(indices, 1)

        var helperRoot = this._helper = new THREE.Group();

        var bb = new THREE.Box3();
        var color = 0xffffff * Math.random();

        var segFunc = this._createSegments.bind(this);

        function createLevel(i, levels, tree) {
            let min = tree.i.min, max = tree.i.max;

            bb.setFromArray(min.concat(max));

            helperRoot.add(segFunc(bb, color));

            if (tree.n) {
                color = 0xffffff * Math.random();
                Utils.forEach(tree.n, function (node) {
                    createLevel(i + 1, levels, node);
                });
            }
        }

        if (bxh) createLevel(0, this._levels, bxh);
    }

    _createSegments(bounds, color) {
        var min = bounds.min, max = bounds.max;

        var positions = new Float32Array(8 * 3);

        var geometry = new THREE.BufferGeometry();
        geometry.setIndex(this._indiceAttribute);
        geometry.addAttribute('position', new THREE.BufferAttribute(positions, 3));

        var position = geometry.attributes.position;
        var array = position.array;

        array[0] = max.x; array[1] = max.y; array[2] = max.z;
        array[3] = min.x; array[4] = max.y; array[5] = max.z;
        array[6] = min.x; array[7] = min.y; array[8] = max.z;
        array[9] = max.x; array[10] = min.y; array[11] = max.z;
        array[12] = max.x; array[13] = max.y; array[14] = min.z;
        array[15] = min.x; array[16] = max.y; array[17] = min.z;
        array[18] = min.x; array[19] = min.y; array[20] = min.z;
        array[21] = max.x; array[22] = min.y; array[23] = min.z;

        position.needsUpdate = true;

        return new THREE.LineSegments(geometry, new THREE.LineBasicMaterial({ color: color }));
    }
}

/**
 * Partition interface
 */
class Partition {
    /**
     * Constructor for partition.
     * @param {Boolean} helper Whether or not to to initialize helper.
     */
    constructor(helper = false) {
        this._root = null;
        this._partitioned = false;
        this._helper = helper ? new THREE.Group() : null;
        this._notInitializedPrinted = false;
    }

    /**
     * Create tree from object.
     * @param {THREE.Object3D} object Object to construct partition from.
     * @param {Funtion} onComplete Callback called after partitioning is complete.
     * @param {Number} minLeaf Min leaf count.
     * @param {Number} maxLeaft Max leaf count.
     */
    build(object, onComplete = null, minLeaf = 2, maxLeaf = 4) {
        const nodes = [];

        function addNode(object, out) {
            if (object instanceof THREE.Mesh && object.isSelectable) {
                let filter = new MeshFilter(object);
                nodes.push(new NodeBxH(object, filter));
            }

            Utils.forEach(object.children, function (object) {
                addNode(object, out);
            }, this);
        }

        object.updateMatrixWorld();
        const dt = new Date().getTime();
        addNode(object, nodes);

        const _onComplete = function () {
            this._partitioned = false;

            if (this._helper) {
                let rebuilt = new BxVHelper(this._root._T);
                this._helper.add(rebuilt._helper);
            }

            if (onComplete) onComplete();
        }.bind(this);

        if (nodes.length > 0) {
            this._root = new BVH(3, minLeaf, maxLeaf);
            this._root.buildFromArrayOfElementsAsync(nodes, null, _onComplete);
        } else {
            console.warn("No nodes, partition could not be built.");
        }
    }

    /**
     * Checks if partition has helper.
     * @returns {Boolean} Whether or not partition was initialized to allow helpers.
     */
    hasHelper() {
        return !!this._helper;
    }

    /**
     * @returns { THREE.Group }
     */
    get helper() {
        if (!this._helper) {
            console.warn("Initialize partition to allow helper.");
        }

        return this._helper;
    }

    /**
     * Create partition.
     */
    destroy() {
        this._root = null;
        this._partitioned = false;

        if (this._helper) this._helper.children = [];
        this._notInitializedPrinted = false;
    }

    /**
     * Intersect partition with sphere to filter unwanted objects.
     * @param {THREE.Sphere} sphere Sphere to test with.
     */
    intersectSphere(sphere) {
        if (!this._root || !this._root._T) {
            if (this._notInitializedPrinted) {
                console.warn("Partition not initialized.");
                this._notInitializedPrinted = true;
            }
            return [];
        }

        var currentLeaves = [this._root._T];
        var nextLeaves = [];
        var nodeLeaves = [];

        while (currentLeaves.length > 0) {
            let leaf = currentLeaves.pop();

            if (!leaf.i._bbox) {
                let min = leaf.i.min, max = leaf.i.max;
                leaf.i._bbox = new THREE.Box3().setFromArray(min.concat(max));
            }

            let itscts = sphere.intersectsBox(leaf.i._bbox);

            if (itscts) {
                if (leaf.n) nextLeaves.push.apply(nextLeaves, leaf.n);
                else if (leaf.o) nodeLeaves.push.apply(nodeLeaves, leaf.o);
            }

            if (currentLeaves.length === 0 && nextLeaves.length > 0) {
                currentLeaves.push.apply(currentLeaves, nextLeaves);
                while (nextLeaves.pop()) { };
            }
        }

        var potentialObjects = [];
        while (nodeLeaves.length > 0) {
            let leaf = nodeLeaves.pop();

            if (sphere.intersectsBox(leaf.o._bbox)) {
                potentialObjects.push(leaf.o._object);
            }
        }

        return potentialObjects;
    }

    /**
     * Performs intersection with boundries from current ray.
     * @returns {Array} Intersected bound nodes.
     */
    intersect() {
        if (!this._root || !this._root._T) {
            if (this._notInitializedPrinted) {
                console.warn("Partition not initialized.");
                this._notInitializedPrinted = true;
            }
            return null;
        }

        var raycaster = Physics.getRaycaster();

        var ray = new Ray(raycaster.ray.origin.toArray(), raycaster.ray.direction.toArray());
        ray.minT = Number.isFinite(raycaster.near) ? raycaster.near : 0;
        ray.maxT = Number.isFinite(raycaster.far) ? raycaster.far : Number.MAX_SAFE_INTEGER;

        var intersectInfo = new IntersectInfo();
        intersectInfo.ray = ray;
        intersectInfo.isHit = false;
        intersectInfo.tests = intersectInfo.hits = 0;
        intersectInfo.nodes = [];

        this._root.intersect(ray, intersectInfo);

        Debug.setValue("Intersect tests", intersectInfo.tests);
        Debug.setValue("Intersect hits", intersectInfo.hits);

        return intersectInfo.nodes;
    }
}

export { Partition };