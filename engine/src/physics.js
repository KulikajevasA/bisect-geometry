import THREE from "threejs";
import { Utils } from "../utils";
import { MeshFilter } from "./mesh-filter";

/**
 * Physics. This is a singleton class.
 */
class Physics {
    constructor() {
        this._raycaster = new THREE.Raycaster();
        this._plane = new THREE.Plane();
    }

    /**
     * Sets raycaster from camera.
     * @param {THREE.Vector2} coords Ray start coordinates.
     * @param {THREE.Camera} camera Camera to set from. 
     * @param {Number} near Ray near.
     * @param {Number} far Ray far.
     */
    setFromCamera(coords, camera, near = 0, far = Infinity) {
        this._raycaster.setFromCamera(coords, camera);
        this._raycaster.near = near;
        this._raycaster.far = far;
    }

    /**
     * Gets intersection with plane.
     * @param {THREE.Vector3} normal Plane normal. 
     * @param {Number} constant Plane constant.
     */
    getPlaneIntersection(normal, constant = 0) {
        this._plane.set(normal, constant);

        return this._raycaster.ray.intersectsPlane(this._plane);
    }

    /**
     * Set raycaster values.
     * @param {THREE.Vector3} origin Ray origin.
     * @param {THREE.Vector3} dir Ray direction.
     * @param {Number} near Ray near.
     * @param {Number} far Ray far.
     */
    setRay(origin, dir, near = 0, far = Infinity) {
        this._raycaster.set(origin, dir);
        this._raycaster.near = near;
        this._raycaster.far = far;
    }

    /**
     * Getter for raycaster.
     * @returns {THREE.Raycaster} Raycaster.
     */
    getRaycaster() {
        return this._raycaster;
    }

    /**
     * Intersects sphere with mesh.
     * @param {THREE.Sphere} sphere Sphere to intersect. 
     * @param {THREE.Mesh} mesh Mesh to intersect.
     */
    _intersectSphereMesh(sphere, mesh) {
        var tri = new THREE.Triangle();
        var plane = new THREE.Plane();
        var projectedCenter = new THREE.Vector3(), bary = new THREE.Vector3();
        var vA = new THREE.Vector3();
        var vB = new THREE.Vector3();
        var vC = new THREE.Vector3();
        var ray = new THREE.Ray();
        var distRayToCenterSq = Infinity;
        var raySphereItsctPos = new THREE.Vector3();
        var radSq = sphere.radius * sphere.radius;
        var min = null;

        function checkIntersection(sphere, object, pA, pB, pC) {
            tri.set(pA, pB, pC);
            tri.plane(plane);

            plane.projectPoint(sphere.center, projectedCenter);

            if (tri.containsPoint(projectedCenter)) {
                var dist = projectedCenter.distanceToSquared(sphere.center);

                //Head on collision
                if (dist < sphere.radius * sphere.radius) {
                    return {
                        distance: dist,
                        point: projectedCenter,
                        object: object
                    };
                }
            }

            //Check if triangle edge rays intersects sphere and find closest edge to center.
            ray.origin.copy(pB);
            ray.direction.subVectors(pC, ray.origin).normalize();
            var itsctSphere = ray.intersectSphere(sphere, raySphereItsctPos);
            if (itsctSphere) {
                distRayToCenterSq = ray.distanceSqToPoint(sphere.center);

                if (distRayToCenterSq < radSq) {
                    min = {
                        distance: distRayToCenterSq,
                        point: raySphereItsctPos,
                        object: object
                    };
                }
            }

            ray.origin.copy(pC);
            ray.direction.subVectors(pA, ray.origin).normalize();
            itsctSphere = ray.intersectSphere(sphere, raySphereItsctPos);
            if (itsctSphere) {
                distRayToCenterSq = ray.distanceSqToPoint(sphere.center);

                if (distRayToCenterSq < radSq && (!min || min.distance > distRayToCenterSq)) {
                    min = {
                        distance: distRayToCenterSq,
                        point: raySphereItsctPos,
                        object: object
                    };
                }
            }

            ray.origin.copy(pA);
            ray.direction.subVectors(pB, ray.origin).normalize();
            itsctSphere = ray.intersectSphere(sphere, raySphereItsctPos);

            if (itsctSphere) {
                distRayToCenterSq = ray.distanceSqToPoint(sphere.center);

                if (distRayToCenterSq < radSq && (!min || min.distance > distRayToCenterSq)) {
                    min = {
                        distance: distRayToCenterSq,
                        point: raySphereItsctPos,
                        object: object
                    };
                }
            }

            if (min) {
                min.distance = Math.sqrt(min.distance);
            }

            return min;
        };

        function checkBufferGeometryIntersection(sphere, object, position, a, b, c) {

            vA = new THREE.Vector3().fromBufferAttribute(position, a);
            vB = new THREE.Vector3().fromBufferAttribute(position, b);
            vC = new THREE.Vector3().fromBufferAttribute(position, c);

            return checkIntersection(sphere, object, vA, vB, vC);
        }

        var intersection;

        var geometry = mesh.geometry;
        var intersects = [];

        if (geometry.isBufferGeometry) {

            var a, b, c;
            var index = geometry.index;
            var position = geometry.attributes.position;
            var i, l;

            if (index !== null) {

                // indexed buffer geometry

                for (i = 0, l = index.count; i < l; i += 3) {

                    a = index.getX(i);
                    b = index.getX(i + 1);
                    c = index.getX(i + 2);

                    intersection = checkBufferGeometryIntersection(sphere, mesh, position, a, b, c);

                    if (intersection) {
                        intersects.push(intersection);
                    }

                }

            } else {

                // non-indexed buffer geometry

                for (i = 0, l = position.count; i < l; i += 3) {

                    a = i;
                    b = i + 1;
                    c = i + 2;

                    intersection = checkBufferGeometryIntersection(sphere, mesh, position, a, b, c);

                    if (intersection) {
                        intersects.push(intersection);
                    }

                }

            }

        } else if (geometry.isGeometry) {

            var fvA, fvB, fvC;
            var isMultiMaterial = Array.isArray(material);

            var vertices = geometry.vertices;
            var faces = geometry.faces;
            var uvs;

            var faceVertexUvs = geometry.faceVertexUvs[0];
            if (faceVertexUvs.length > 0) uvs = faceVertexUvs;

            for (var f = 0, fl = faces.length; f < fl; f++) {

                var face = faces[f];
                var faceMaterial = isMultiMaterial ? material[face.materialIndex] : material;

                if (faceMaterial === undefined) continue;

                fvA = vertices[face.a];
                fvB = vertices[face.b];
                fvC = vertices[face.c];

                if (faceMaterial.morphTargets === true) {

                    var morphTargets = geometry.morphTargets;
                    var morphInfluences = this.morphTargetInfluences;

                    vA.set(0, 0, 0);
                    vB.set(0, 0, 0);
                    vC.set(0, 0, 0);

                    for (var t = 0, tl = morphTargets.length; t < tl; t++) {

                        var influence = morphInfluences[t];

                        if (influence === 0) continue;

                        var targets = morphTargets[t].vertices;

                        vA.addScaledVector(tempA.subVectors(targets[face.a], fvA), influence);
                        vB.addScaledVector(tempB.subVectors(targets[face.b], fvB), influence);
                        vC.addScaledVector(tempC.subVectors(targets[face.c], fvC), influence);

                    }

                    vA.add(fvA);
                    vB.add(fvB);
                    vC.add(fvC);

                    fvA = vA;
                    fvB = vB;
                    fvC = vC;

                }

                intersection = checkIntersection(sphere, object, fvA, fvB, fvC);

                if (intersection) {
                    intersects.push(intersection);
                }

            }
        }

        return intersects;
    }

    /**
     * Intersects sphere with objects.
     * @param {THREE.Sphere} sphere Sphere to intersect.
     * @param {Array} objects Objects to intersect sphere with.
     * @param {Boolean} recursive Is recursive.
     */
    intersectSphereToObjects(sphere, objects = [], recursive = false) {
        var intersections = [];

        objects = objects instanceof Array ? objects : [objects];

        function tryIntersect(sphere, objects) {
            Utils.forEach(objects, function (object) {
                if (object instanceof THREE.Mesh) {
                    let itsct = this._intersectSphereMesh(sphere, object);

                    intersections.push.apply(intersections, itsct);
                }

                if (recursive) tryIntersect.call(this, sphere, object.children);
            }, this);
        };

        tryIntersect.call(this, sphere, objects);

        intersections.sort((a, b) => {
            return a.distance - b.distance;
        });

        return intersections;
    }

    /**
     * Intersects triangles for filtered meshes.
     * @param {MeshFilter[]} filters Mesh filters.
     */
    intersectFiltered(filters) {
        var hits = [];

        for (let i = 0; i < filters.length; i++) {
            let filter = filters[i];
            let itsct = filter.intersect(this._raycaster);

            if (itsct) hits.push(itsct);
        }

        hits.sort((a, b) => {
            return a.distance - b.distance;
        });

        return hits;
    }
};

if (!Physics._instance) {
    Physics._instance = null;
    Physics.getInstance = function () {
        if (!Physics._instance) {
            Physics._instance = new Physics();
        }

        return Physics._instance;
    };
}

const instance = Physics.getInstance();

export { instance as Physics };