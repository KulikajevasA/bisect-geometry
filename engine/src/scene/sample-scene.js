import THREE from "threejs";
import { Debug } from "../../debug";
import { Utils } from "../../utils";
import { Physics } from "../physics";
import { EventManager } from "../events/events-man";
import { MouseButtons_T } from "../input/mouse";
import { Scene } from "./scene";

/**
 * A sample scene.
 */
class SampleScene extends Scene {
    constructor(manager) {
        super(manager);

        this._activeCamera.position.set(150, 150, 150);
        this._activeCamera.lookAt(new THREE.Vector3());
        this._controls.update();

        this._test();
    }

    _test() {
        const cb1 = this._addTestCube(0.5, 0xff);
        const cb2 = this._addTestCube(0.5);
        const cb3 = this._addTestCube(1, 0xff00ff);

        cb2.position.set(-1, 1, 1);
        cb3.position.set(0, 0, 0.5);

        cb3.add(cb2);

        const cubes = 1000, posVar = 100, scaleVar = 3;
        for (let i = 0; i < cubes; i++) {
            const cube = this._addTestCube(Math.random() * scaleVar, 0xffffff * Math.random());
            cube.position.set(Math.random() * posVar,
                Math.random() * posVar,
                Math.random() * posVar);

            this.add(cube);
        }

        Debug.setValue("Objects", cubes + 3);

        // this.partition();

        EventManager.on("postdraw", function () {
            const state = this._manager.engine.inputManager.state;
            const mouse = state.mouse;

            if (mouse.states[MouseButtons_T.LEFT]) {
                Physics.getRaycaster().setFromCamera(mouse.position, this.camera);
                const nodes = this._partition.intersect();

                if (nodes && nodes.length > 0) {
                    const intersections = Physics.getRaycaster().intersectObjects(nodes, true);

                    if (intersections.length > 0) {
                        intersections[0].object.material.color.setHex(0xffffff * Math.random());
                    }

                }
            }
        }.bind(this));
    }
}

export { SampleScene };