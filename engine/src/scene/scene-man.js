import { Engine } from "../engine";
import { Scene } from "./scene";
import { EventManager } from "../events/events-man";
import { CustomEvent } from "../events/custom-event";

/**
 * Scene manager.
 */
class SceneManager {
    /**
     * Scene manager constructor.
     * @param {Engine} engine Engine instance.
     */
    constructor(engine) {
        this._engine = engine;

        this._activeScene = null;

        this._init();
    }

    /**
     * Getter for current active scene.
     * @returns {Scene} Current active scene.
     */
    get scene() {
        return this._activeScene;
    }

    /**
     * Setter for current active scene.
     * @param {Scene} scene Set active scene.
     */
    set scene(scene) {
        this._activeScene = scene;
    }

    /**
     * Getter for engine.
     * @returns {Engine} Engine.
     */
    get engine() {
        return this._engine;
    }

    /**
     * Self-initializer.
     */
    _init() {
        EventManager.on("resize", this._handleResize.bind(this));
    }

    /**
     * Handler for resize event.
     * @param {CustomEvent} evt 
     */
    _handleResize(evt) {
        if (this.scene)
            this.scene.updateAspect(evt.custom.width / evt.custom.height);
    }
}

export { SceneManager, Scene };