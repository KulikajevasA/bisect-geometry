import THREE from "threejs";
import { Config } from "../../config";
import { Debug } from "../../debug";
import { SceneManager } from "./scene-man";
import { Utils } from "../../utils";
import { Physics } from "../physics";
import { Partition } from "../partition";
import { EventManager } from "../events/events-man";

/**
 * Scene.
 */
class Scene extends THREE.Scene {
    /**
     * Constructor for the scene.
     * @param {SceneManager} manager Scene manager that is handling this scene. 
     */
    constructor(manager) {
        super();

        this._manager = manager;

        const confCamera = Config.CAMERA;

        this._activeCamera = new THREE.PerspectiveCamera(confCamera.FOV, 1, confCamera.NEAR, confCamera.FAR);
        this.add(this._activeCamera);

        this.camera.position.copy(confCamera.DEFAULT_POSITION);
        this.camera.lookAt(confCamera.DEFAULT_TARGET);

        this._controls = this._initControls();

        this._intersectObjects = [];

        this._partition = this._initPartition();
        if (this._partition.hasHelper()) this.add(this._partition.helper);

    }

    /**
     * Initializes the controls for scene.
     */
    _initControls() {
        return new THREE.OrbitControls(this._activeCamera, this._manager.engine.params.wrappers.viewport);
    }

    /**
     * Initializes the partition for scene.
     */
    _initPartition() {
        return new Partition();
    }

    /**
     * Partition the world.
     */
    partition() {
        this._partition.destroy();
        this._partition.build(this);
    }


    /**
     * Gets partition.
     * @returns {Partition} partition.
     */
    getPartition() {
        return this._partition;
    }

    /**
     * Adds a test cube.
     * @param {Number} size Size of the cube. (Default: 1)
     * @param {Number} color Color.
     * @returns {THREE.Mesh} Created cube mesh.
     */
    _addTestCube(size = 1, color = 0xffffff) {
        const cube = new THREE.Mesh(
            //new THREE.SphereBufferGeometry(size, 8, 8),
            new THREE.BoxBufferGeometry(size, size, size),
            new THREE.MeshBasicMaterial({ color: color, visible: true })
        );

        this.add(cube);

        return cube;
    }

    /**
     * Getter for current active camera.
     * @returns {THREE.Camera} Current active camera.
     */
    get camera() {
        return this._activeCamera;
    }

    /**
     * Updates aspect ratio for active camera.
     */
    updateAspect(aspect) {
        if (this.camera) {
            this.camera.aspect = aspect;
            this.camera.updateProjectionMatrix();
        }
    }
}

export { Scene };