import { EventManager } from "./events/events-man";
import { Utils } from "../utils";
import { Debug } from "../debug";
import { Engine } from "./engine";

/**
 * Performs WebGL rendering tasks.
 */
class View3d {
    /**
     * Performs rendering tasks.
     * @param {Engine} engine Engine instance.
     */
    constructor(engine) {
        this._engine = engine;

        this._renderer = null;
        this._canvas = null;

        this._isRendering = false;

        this._elementRect = null;

        Debug.show();
    }

    /**
     * Sets clear color.
     * @param {THREE.Color|Number|String} color Clear color.
     * @param {Number} alpha Alpha.
     */
    setClearColor(color, alpha) {
        this._renderer.setClearColor(color, alpha);
    }

    /**
     * Initializes the renderer.
     */
    init() {
        try {
            const params = this._engine.params.rendering || {};

            this._renderer = new THREE.WebGLRenderer(params);

            this._canvas = this._renderer.domElement;
            this._engine.params.wrappers.viewport.appendChild(this._canvas);

            window.addEventListener("resize", this._onResize.bind(this), false);

            this._onResize();
        } catch (e) {
            console.error("ERROR: WebGL initialization failed. Reason:", e);
            return false;
        }

        return true;
    }

    /**
     * Forces viewport to adapt to size.
     */
    forceAdaptViewportSize() {
        this._onResize(null);
    }

    /**
     * Event handler for 'resize' event.
     * @param {Event} evt  Event handler data.
     */
    _onResize(evt) {
        const p = Utils.getElementRect(this._engine.params.wrappers.viewport);

        this._elementRect = p;

        this._renderer.setSize(p.width, p.height);

        Debug.setValue("Viewport", "{0}x{1}".format(p.width, p.height));

        EventManager.trigger("resize", evt, p);
    }

    /**
     * Getter for viewport size.
     * @returns {THREE.Vector2} Viewport size.
     */
    get size() {
        return new THREE.Vector2(this._elementRect.width, this._elementRect.height);
    }

    /**
     * Renders a frame.
     */
    _render() {
        Debug.setValue("FPS", 0);


        if (this._isRendering) {
            const activeScene = this._engine.sceneManager.scene;
            const activeCamera = activeScene ? activeScene.camera : null;

            if (!activeScene || !activeCamera) {

                this._isRendering = false;
                return;
            }

            const begin = new Date().getTime();

            EventManager.trigger("predraw", null, {
                begin: begin,
                camera: activeCamera,
                scene: activeScene
            });

            this._renderer.clear(true, true, true);

            EventManager.trigger("draw", null, {
                begin: begin,
                camera: activeCamera,
                scene: activeScene
            });
            this._renderer.render(activeScene, activeCamera);

            const end = new Date().getTime();

            const dt = end - begin;

            EventManager.trigger("postdraw", null, {
                begin: begin,
                end: end,
                dt: dt,
                camera: activeCamera,
                scene: activeScene
            });

            requestAnimationFrame(this._render.bind(this));

            const fps = getFPS();

            Debug.setValue("FPS", fps);
        }

        Debug.render();
    }

    /**
     * Returns wheter or not rendering is running.
     * @returns {Boolean} Is rendering?
     */
    get isRendering() {
        return this._isRendering;
    }

    /**
     * Starts rendering.
     */
    beginRendering() {
        this._isRendering = true;
        this._render();
        this.forceAdaptViewportSize();
    }

    /**
     * Stops rendering.
     */
    stopRendering() {
        this._isRendering = false;
    }
}

const getFPS = (function () {
    let lastLoop = (new Date()).getMilliseconds();
    let count = 1;
    let fps = 0;

    return function () {
        const currentLoop = (new Date()).getMilliseconds();
        if (lastLoop > currentLoop) {
            fps = count;
            count = 1;
        } else count += 1;

        lastLoop = currentLoop;
        return fps;
    };
}());

export { View3d };