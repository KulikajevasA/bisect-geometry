import * as THREE from "three";

global.THREE = THREE;

if (global.THREE.OrbitControls === undefined)
    require("three/examples/js/controls/OrbitControls");

if (global.THREE.LoaderSupport === undefined)
    require("three/examples/js/loaders/LoaderSupport");

if (global.THREE.OBJLoader === undefined)
    require("three/examples/js/loaders/OBJLoader");

if (global.THREE.OBJLoader2 === undefined)
    require("three/examples/js/loaders/OBJLoader2");

if (global.THREE.MTLLoader === undefined)
    require("three/examples/js/loaders/MTLLoader");

let _window = window;
while (_window !== _window.parent) { _window = _window.parent; }

_window.THREE = THREE;
_window.APP = window;
window.APP = window;

export default THREE;
export { THREE };