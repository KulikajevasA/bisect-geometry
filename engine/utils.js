var Utils = {
    getElementRect(element) {
        var width = element.offsetWidth;
        var height = element.offsetHeight;

        var top = 0;
        var left = 0;

        while (element != null) {
            top += element.offsetTop;
            left += element.offsetLeft;
            element = element.offsetParent;
        }

        var top_scroll = this.getScroll("Top", document.body);

        top -= top_scroll;

        return { top: top, left: left, width: width, height: height };
    },

    getScroll: function (method, element) {
        // The passed in `method` value should be 'Top' or 'Left'
        //method = 'scroll' + method;
        var scrOfX = 0, scrOfY = 0;

        if (typeof (window.pageYOffset) == 'number') {

            //Netscape compliant
            scrOfY = window.pageYOffset;
            scrOfX = window.pageXOffset;
        } else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
            //DOM compliant
            scrOfY = document.body.scrollTop;
            scrOfX = document.body.scrollLeft;
        } else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
            //IE6 standards compliant mode
            scrOfY = document.documentElement.scrollTop;
            scrOfX = document.documentElement.scrollLeft;
        }

        return method == 'Top' ? scrOfY : scrOfX;
    },

    setXYPosition: function (evt, pos) {
        pos.x = evt.clientX - pos.view_l;
        pos.y = evt.clientY - pos.view_t;

        pos.nx = ((2 * pos.x) / pos.view_w - 1);
        pos.ny = (-(2 * pos.y) / pos.view_h + 1);// ndc.y;
    },

    forEach: function (obj, callback, bind) {
        if (!callback) return;

        if (obj instanceof Array) {
            for (var i = 0; i < obj.length; i++) {
                callback.call(bind || obj, obj[i], i)
            };
        } else {
            for (var p in obj) {
                callback.call(bind || obj, obj[p], p)
            };
        }
    },

    __QUERY_PARAMS: null,
    get queryParams() {
        if (this.__QUERY_PARAMS) return this.__QUERY_PARAMS;

        var match,
            pl = /\+/g,  // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
            query = window.location.search.substring(1);

        this.__QUERY_PARAMS = {
            values: {},
            has: function (param) {
                return this.values[param] !== undefined && this.values[param] !== "false" && this.values[param] !== 0;
            },
            get: function (param) {
                return this.values[param] || null;
            }
        };

        while (match = search.exec(query))
            this.__QUERY_PARAMS.values[decode(match[1])] = decode(match[2]);

        return this.__QUERY_PARAMS;
    }
};

export { Utils };
export default Utils;