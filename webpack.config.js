const path = require("path");

const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: ["babel-polyfill", "./app/app.js"],
    output: {
        path: path.resolve("bin"),
        filename: "engine/app.bundle.js",
    },
    watch: false,
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            include: [],
            loader: "babel-loader",
        }, /*{
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            loader: "source-map-loader"
        },*/ {
            test: /\.(png|jpg|bmp)$/,
            loader: "file-loader?outputPath=img/"
        }, {
            test: /\.(json|obj|fbx|mtl)$/,
            //include: "/src/model3d/",
            loader: "file-loader?outputPath=model3d/"
        }, {
            test: /\.(frag|vert|vs|fs)$/,
            loader: "raw-loader"
        }, {
            test: /\.typeface$/,
            loader: "json-loader"
        }, {
            test: /\.css$/,
            use: ["style-loader", {
                loader: "css-loader",
                options: {
                    url: false
                }
            }]
        }]
    },
    plugins: [
        new CopyWebpackPlugin([
            { from: "./app/css", to: "css" },
            { from: "./app/index.html", to: "index.html" },
 

            { from: "./engine/css", to: "engine/css" },
            { from: "./engine/engine.html", to: "engine/engine.html" },
        ]),
    ],
    resolve: {
        alias: {
            // // You can use this to override some packages and use local versions
            "engine": path.resolve(__dirname + "/engine/app.js"),
            "threejs": path.resolve(__dirname + "/engine/threejs.js"),
        },
    },
    devtool: "eval-source-map",
    // devtool: "source-map",
    devServer: {
        port: 8082
    },
}